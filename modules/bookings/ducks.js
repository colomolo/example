import { fetchBookings as callFetchBookings } from '../api';
import { MODULE_NAME } from './constants';

export const DUCKS_NAME = 'bookings';

const actionTypes = {
  FETCH_BOOKINGS_START: `${MODULE_NAME}/${DUCKS_NAME}/FETCH_BOOKINGS_START`,
  FETCH_BOOKINGS_SUCCESS: `${MODULE_NAME}/${DUCKS_NAME}/FETCH_BOOKINGS_SUCCESS`,
  FETCH_BOOKINGS_FAILURE: `${MODULE_NAME}/${DUCKS_NAME}/FETCH_BOOKINGS_FAILURE`,
  FILTER_BOOKINGS: `${MODULE_NAME}/${DUCKS_NAME}/FILTER_BOOKINGS`,
};

const initialState = {
  isLoading: false,
  hasFailed: false,
  data: null,
  filterStr: '',
  isFiltered: false,
  filteredBookings: null,
};

export const reducer = (state = initialState, action) => {
  if (action.type === actionTypes.FETCH_BOOKINGS_START) {
    return {
      ...state,
      isLoading: true,
      hasFailed: false,
      data: null,
    };
  }

  if (action.type === actionTypes.FETCH_BOOKINGS_SUCCESS) {
    return {
      ...state,
      isLoading: false,
      hasFailed: false,
      data: [...action.payload],
    };
  }

  if (action.type === actionTypes.FILTER_BOOKINGS) {
    return {
      ...state,
      filterStr: action.filterStr,
      isFiltered: !!action.filterStr,
      filteredBookings: action.filteredBookings,
    }
  }

  return state;
};

export const fetchBookings = () => async dispatch => {
  try {
    dispatch({ type: actionTypes.FETCH_BOOKINGS_START });
    const bookings = await callFetchBookings();
    dispatch({ type: actionTypes.FETCH_BOOKINGS_SUCCESS, payload: bookings });
  } catch (e) {
    dispatch({ type: actionTypes.FETCH_BOOKINGS_FAILURE });
  }
};

export const filterBookings = (filterStr, bookings) => dispatch => {
  const filterRx = new RegExp(`^${filterStr}`, 'i');
  const filteredBookings = bookings
    .filter((booking) => {
      const nameParts = booking.guestName.split(/\s/);

      return nameParts.some(part => filterRx.test(part))
    });

  dispatch({
    type: actionTypes.FILTER_BOOKINGS,
    filterStr,
    filteredBookings,
  });
}
