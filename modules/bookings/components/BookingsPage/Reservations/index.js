import React from 'react';

import CalendarLogo from '../../../../../static/calendar.svg';
import globalStyles from '../../../../core/global.css';
import styles from './styles.css';

export class Reservations extends React.PureComponent {
  static displayName = 'Reservations';

  render() {
    return (
      <div className={ styles['reservations'] }>
        <CalendarLogo />
        <h1>Reservations</h1>
        <p>Select any reservation item</p>
        <button className={ `${globalStyles['secondary']} ${globalStyles['wide']}` }>Create Booking</button>
        <button className={ `${globalStyles['secondary']} ${globalStyles['wide']}` }>Create Booking with Quote</button>
      </div>
    );
  }
}
