import React from 'react';

import styles from './styles.css';

import { BookingsList } from './BookingsList'
import { Reservations } from './Reservations'

export class BookingsPage extends React.PureComponent {
  static displayName = 'BookingsPage';

  render() {
    return (
      <div className={ styles['bookings-page'] }>
        <BookingsList />
        <div className={ styles['bookings-page__workzone'] }>
          <Reservations />
        </div>
      </div>
    );
  }
}
