import React from 'react';
import { func } from 'prop-types';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faArrowDown, faSyncAlt } from '@fortawesome/free-solid-svg-icons';

import globalStyles from '../../../../../core/global.css';
import styles from './styles.css';

export class BookingsActions extends React.PureComponent {
  static displayName = 'BookingsActions';
  static propTypes = {
  };

  constructor(props) {
    super(props);

    this.state = { searchStr: '' };

    this.handleInput = this.handleInput.bind(this);
  }

  handleInput(e) {
    this.setState({ searchStr: e.target.value });

    this.props.filterBy(e.target.value);
  }

  render() {
    return (
      <div className={ styles['bookings-actions'] }>
        <div className={ styles['bookings-actions__bar'] }>
          Select
          <FontAwesomeIcon icon={ faArrowDown } />
          <FontAwesomeIcon icon={ faSyncAlt } />
        </div>
        <div className={ styles['bookings-actions__create-button-wrapper'] }>
          <button className={ `${globalStyles['primary']} ${globalStyles['wide']}` }>Create booking</button>
        </div>
      </div>
    );
  }
}
