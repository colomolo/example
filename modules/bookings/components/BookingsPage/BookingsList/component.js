import React from 'react';
import { func, bool, arrayOf, shape, string } from 'prop-types';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faFilter } from '@fortawesome/free-solid-svg-icons';

import styles from './styles.css';

import { BookingItem } from './BookingItem'
import { BookingsSearch } from './BookingsSearch'
import { BookingsActions } from './BookingsActions'

export class BookingsList extends React.PureComponent {
  static displayName = 'BookingsList';
  static propTypes = {
    fetchBookings: func.isRequired,
    isLoading: bool,
    hasFailed: bool,
    data: arrayOf(shape({ guestName: string })),
    filteredBookings: arrayOf(shape({ guestName: string })),
  };

  componentDidMount() {
    this.props.fetchBookings();
  }

  render() {
    const bookings = this.props.isFiltered ? this.props.filteredBookings : this.props.data

    return (
      <div className={ styles['bookings-list__wrapper'] }>
        { this.props.isLoading &&
          <div className={ styles['bookings-list__notice'] }>Loading…</div>
        }

        { this.props.hasFailed &&
          <div className={ styles['bookings-list__notice'] }>Failed :(</div>
        }

        { !this.props.isLoading && !this.props.hasFailed && !!bookings &&
          <div>
            <div className={ styles['bookings-list__filters'] }>
              <BookingsSearch />
              <div className={ styles['bookings-list__filter-icon'] }>
                <FontAwesomeIcon icon={ faFilter } />
              </div>
            </div>
            <div className={ styles['bookings-list'] }>
              { !!bookings &&
                bookings.map(booking => <BookingItem key={ booking.id } booking={ booking } />)
              }

              { this.props.isFiltered && !this.props.filteredBookings &&
                <div className={ styles['bookings-list__notice'] }>Nothing found</div>
              }
            </div>
            <BookingsActions />
          </div>
        }
      </div>
    )
  }
}
