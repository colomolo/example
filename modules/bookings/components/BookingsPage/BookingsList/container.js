import { connect } from 'react-redux';

import { fetchBookings } from '../../../ducks';
import { BookingsList } from './component';

const mapStateToProps = state => ({
  isLoading: state.bookings.isLoading,
  hasFailed: state.bookings.hasFailed,
  data: state.bookings.data,
  isFiltered: state.bookings.isFiltered,
  filteredBookings: state.bookings.filteredBookings,
});

const mapDispathToProps = {
  fetchBookings,
};

export const Container = connect(
  mapStateToProps,
  mapDispathToProps,
)(BookingsList);
