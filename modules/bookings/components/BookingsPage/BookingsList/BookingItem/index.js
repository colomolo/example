import React from 'react';
import { number, bool, shape, object, string } from 'prop-types';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faReply, faMoon, faUser } from '@fortawesome/free-solid-svg-icons';
import cx from 'classnames';

import styles from './styles.css';

import { formatDateCreated, formatDateArrival, calcNightsToStay } from '../../../../../../lib/timeUtils'

export class BookingItem extends React.PureComponent {
  static displayName = 'BookingItem';
  static propTypes = {
    booking: shape({
      amountPaid: number,
      currencyCode: string,
      guestName: string,
      dateArrival: object,
      dateCreated: object,
      dateDeparture: object,
      people: number,
      propertyName: string,
      replied: bool,
      status: string,
    }).isRequired,
  };

  constructor(props) {
    super(props);

    this.booking = this.props.booking;
  }

  render() {
    const statusClassName = cx(
      styles['booking-item__status'],
      [styles[`booking-item__status_${this.booking.status.toLowerCase()}`]],
    );
    
    return (
      <div className={ styles['booking-item'] }>
        <div className={ statusClassName } />

        { this.booking.replied &&
          <div className={ styles['booking-item__replied'] }>
            <FontAwesomeIcon icon={ faReply } />
          </div>
        }

        <div className={ styles['booking-item__guest-name'] }>
          { this.booking.guestName }
        </div>
        <div className={ styles['booking-item__created-date'] }>
          { formatDateCreated(this.booking.dateCreated) }
        </div>
        <div className={ styles['booking-item__property-name'] }>
          { this.booking.propertyName }
        </div>
        <div className={ styles['booking-item__meta'] }>
          <span className={ styles['booking-item__arrival-date'] }>
            { formatDateArrival(this.booking.dateArrival) }
            , </span>
          <span className={ styles['booking-item__nights-count'] }>
            {
              calcNightsToStay(this.booking.dateArrival, this.booking.dateDeparture)
            } <FontAwesomeIcon icon={ faMoon } />
          </span>
          <span className={ styles['booking-item__guests-count'] }>
            { this.booking.people } <FontAwesomeIcon icon={ faUser } />
          </span>
        </div>
        <div className={ styles['booking-item__paid-amount'] }>
          { `${this.booking.amountPaid} ${this.booking.currencyCode}` }
        </div>
      </div>
    );
  }
}
