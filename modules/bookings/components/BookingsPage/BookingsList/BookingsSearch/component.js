import React from 'react';
import { arrayOf, shape, string, func } from 'prop-types';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faSearch } from '@fortawesome/free-solid-svg-icons';

import styles from './styles.css';

export class BookingsSearch extends React.PureComponent {
  static displayName = 'BookingsSearch';
  static propTypes = {
    data: arrayOf(shape({ guestName: string })),
    filterBookings: func.isRequired,
  };

  constructor(props) {
    super(props);

    this.state = { searchStr: '' };

    this.handleChange = this.handleChange.bind(this);
  }

  handleChange(e) {
    this.setState({ searchStr: e.target.value });

    this.props.filterBookings(e.target.value, this.props.data);
  }

  render() {
    return (
      <div className={ styles['bookings-search'] }>
        <input
          type="text"
          className={ styles['bookings-search__input'] }
          placeholder="Search"
          onChange={ this.handleChange }
          value={ this.state.searchStr }
        />
        <div className={ styles['bookings-search__icon'] }>
          <FontAwesomeIcon icon={ faSearch } />
        </div>
      </div>
    );
  }
}
