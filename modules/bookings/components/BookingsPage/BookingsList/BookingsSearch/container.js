import { connect } from 'react-redux';

import { filterBookings } from '../../../../ducks';
import { BookingsSearch } from './component';

const mapStateToProps = state => ({
  data: state.bookings.data,
  filteredBookings: state.bookings.filteredBookings,
});

const mapDispathToProps = {
  filterBookings,
};

export const Container = connect(
  mapStateToProps,
  mapDispathToProps,
)(BookingsSearch);
