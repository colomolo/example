const DAY_IN_MILLISECONDS = 24 * 60 * 60 * 1000;
const MONTHS = {
  0: 'Jan',
  1: 'Feb',
  2: 'Mar',
  3: 'Apr',
  4: 'May',
  5: 'Jun',
  6: 'Jul',
  7: 'Aug',
  8: 'Sep',
  9: 'Oct',
  10: 'Nov',
  11: 'Dec',
};

export const formatDateCreated = (date) => {
  const month = date.getMonth() + 1;
  const day = date.getDate();
  const year = date.getFullYear();

  return `${month}/${day}/${year}`;
}

export const formatDateArrival = (date) => {
  const month = MONTHS[date.getMonth()];
  const day = date.getDate();
  const year = date.getFullYear();

  return `${month} ${day} ${year}`;
}

export const calcNightsToStay = (arrivalDate, departureDate) => {
  const stayDuration = departureDate.getTime() - arrivalDate.getTime();

  return Math.ceil(stayDuration / DAY_IN_MILLISECONDS) - 1;
}
